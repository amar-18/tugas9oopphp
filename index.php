<?php 

require('Animal.php');
require('Frog.php');
require('Ape.php');

$sheep = new Animal("Shaun");
echo "Name: $sheep->name  <br>"; // "shaun"
echo "Number of Legs: $sheep->legs <br>"; // 2
echo "Cold Blooded (t/f): $sheep->cold_blooded <br><br>"; // false

$sungokong = new Ape("Kera Sakti");
echo "Name: $sungokong->name  <br>"; // "shaun"
echo "Number of Legs: $sungokong->legs <br>"; // 2
echo "Cold Blooded (t/f): $sungokong->cold_blooded <br>"; // false
$sungokong->yell() ; // "Auooo"

$kodok = new Frog("Buduk");
echo "Name: $kodok->name  <br>"; // "shaun"
echo "Number of Legs: $kodok->legs <br>"; // 2
echo "Cold Blooded (t/f): $kodok->cold_blooded <br>"; // false
$kodok->jump() ; // "hop hop"

 ?>